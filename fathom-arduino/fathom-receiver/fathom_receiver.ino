#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

const int MAXPAYLOAD = 9;

// radio(CE Pin, CSN Pin)
RF24 radio(8,7);

const uint64_t pipe[2] = {0xF0F0F0F0E1LL,0xF0F0F0F0D2LL};

uint16_t packet[3];

unsigned long time;
boolean initialized_ping = false;

void setup(void) {
    Serial.begin(57600);
    printf_begin();
    radio.begin();
    //radio.setAutoAck(false);
    //radio.powerUp();
    radio.setRetries(15,15);
    //radio.setPayloadSize(6); // 9 for 6 byte packet, 12 for 6 byte data + SSD, needs to be 14 to stop failing !!
    //radio.setPALevel(RF24_PA_HIGH); // try and set PA to max !   
    //radio.setDataRate(RF24_250KBPS); // 250kbps for best tx/rx
    radio.openWritingPipe(pipe[1]);
    radio.openReadingPipe(1,pipe[0]);
    
    radio.startListening();
    radio.printDetails();
}

void loop(void) {
  if (!initialized_ping) {
    if(radio.available()) {
        //Dump the whole payload
        boolean done = false;
        while(!done){
            done = radio.read(&time, sizeof(unsigned long));
        }
     radio.stopListening();
     radio.write(&time, sizeof(unsigned long));
     initialized_ping = true;
     printf("Sent response %lu\n\r", time);

     radio.startListening();
  }
  }
  else {
    if(radio.available()) {
      boolean done = false;
      while (!done) {
        done = radio.read(packet, sizeof(packet));
      }
      Serial.print(packet[0]);
      Serial.print("\t");
      Serial.print(packet[1]);
      Serial.print("\t");
      Serial.println(packet[2]);
  }
  }
}



