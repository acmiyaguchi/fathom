#=============================================================================#
# Author: Nathan Wong                                                         #
# Date:   04.12.2014                                                          #
#                                                                             #
# Description: fathom arduino processing                                      #
#                                                                             #
#=============================================================================#
set(CMAKE_TOOLCHAIN_FILE cmake/ArduinoToolchain.cmake) # Arduino Toolchain


cmake_minimum_required(VERSION 2.8)
#====================================================================#
#  Setup Project                                                     #
#====================================================================#
project(ArduinoExample C CXX)

print_board_list()
print_programmer_list()

add_subdirectory(fathom)   #add the example directory into build
