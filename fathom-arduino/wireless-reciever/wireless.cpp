#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

RF24 radio(9, 10); // Actual pins to be decided on

const uint64_t pipe = 0xF0F0F0F0E1LL;

uint16_t packet[3];

void setup(void) {
    Serial.begin(115200);
    radio.begin();
    radio.openReadingPipe(1, pipe);
    radio.startListening();
}

void loop(void) {
    if(radio.available()) {
        //Dump the whole payload
        bool done = false;
        while(!done) {
            done = radio.read(packet, sizeof(packet));
            Serial.print(packet[0]);
            Serial.print("\t");
            Serial.print(packet[1]);
            Serial.print("\t");
            Serial.println(packet[2]);
        }
        else {
            Serial.println("No radio available");
        }
    }
}
