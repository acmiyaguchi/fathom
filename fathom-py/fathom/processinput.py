import numpy as np
from scipy import integrate
import teststream

class ProcessInput(): 
    def __init__(self, inputstream):
        self.accelsamples = []
        self.inputstream = inputstream

    def process(self, pullrate):
        for i in range(0, pullrate):
            self.accelsamples.append(np.array(self.inputstream.read_data()))

        self.accelsamples = np.array(self.accelsamples)
        self.accelsamples = self.accelsamples/16384*10

        numSample = range(0, pullrate)
        xaccel = np.zeros(pullrate)
        yaccel = np.zeros(pullrate)
        zaccel = np.zeros(pullrate)

        for n in numSample:
            xaccel[n] = self.accelsamples[n, 0]
            yaccel[n] = self.accelsamples[n, 1]
            zaccel[n] = self.accelsamples[n, 2]

        xvel = integrate.simps(xaccel, numSample)
        yvel = integrate.simps(yaccel, numSample)
        zvel = integrate.simps(zaccel, numSample)	

        #Need to find pullrate-to-time conversion to properly caculate displacement
        displacement = np.array([xvel*pullrate, yvel*pullrate, zvel*pullrate])

        return np.sqrt(displacement.dot(displacement))

if __name__ == "__main__":
    import teststream
    test = teststream.TestStream('test')
    p = ProcessInput(test)
    print("{0}".format(p.process(20)))
