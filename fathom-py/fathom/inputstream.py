import base
import serial

class InputStream(base.InputStreamBase):
    """
        The input stream will read from a serial interface and provide the
        caller with an abstracted view of the imu device. Read_data provides
        data back from the arduino collector over the wireless channel.
    """

    def __init__(self, port, baudrate):
        """ Constructor. """
        self.ser = serial.Serial(port, baudrate)
        #Move until we reach the control signal
        
    def startStream(self):
        text = ''
        while text != '$$$':
            text = self.ser.readline()
        return True
    
    def read_data(self):
        """ Return a tuple representing the three dimensional linear acceleration vector. """
        val = self.ser.readline().split()
        if len(val) == 3:
            return (int(val[0]), int(val[1]), int(val[2]))
        else:
            return None

if __name__ == "__main__":
    test = InputStream('/dev/ttyACM0', 57600)
    for _ in range(0, 20):
        print(test.read_data())

