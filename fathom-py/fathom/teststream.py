import base
import os

class TestStream(base.InputStreamBase):
    """ Read in data from a file """
    
    def __init__(self, filename):
        """ Reads in a test file from the fathom-py/notes directory"""
        self.path = os.path.abspath('../notes/{0}'.format(filename))
        with open(self.path, 'r') as f:
            self.lines = f.readlines()
        self.idx = 0;

    def read_data(self):
        """ Returns a tuple, else None """
        if self.idx < len(self.lines):
            ret = self.lines[self.idx].split()
            self.idx += 1
            return (ret[1], ret[2], ret[3])
        else:
            return None
        
        
if __name__ == "__main__":
    test = TestStream('test')
    item = ()
    while(True):
        item = test.read_data()
        if item is None:
            break
        print("{0}".format(item))

