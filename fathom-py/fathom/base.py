""" 
Provide the base implementations for various classes, to abstract
the nitty gritty details from other components
"""

class InputStreamBase(object):
    """ 
    The InputStreamBase takes an input from a source, for our it should be an 
    serial stream from an arduino interface to the rf24 module. This object should
    help abstract the input stream for the main data processing module. 
	
    Derive from this class to handle an input stream from a different source.
    """

    def __init__(self):
        """ Constructor. """
        pass

    def read_data(self):
        """ 
        Routine returns a tuple of 3 values, (x, y, z) linear
        acceration values in 16 bit precision. The function will act as
        a streaming device, and should be buffered.
        """
        pass

