import sys
from PyQt4 import QtGui
from PyQt4 import QtCore

class MainWindow(QtGui.QWidget):

	def __init__(self):
		super(MainWindow, self).__init__()

		self.distanceLabel = QtGui.QLabel("Distance:")
		self.dataList = QtGui.QListWidget()
		self.aboutButton = QtGui.QPushButton("About")

		self.hLayout = QtGui.QHBoxLayout()
		self.hLayout.addWidget(self.distanceLabel)

		self.vLayout = QtGui.QVBoxLayout()
		self.vLayout.addWidget(self.dataList)
		self.vLayout.addWidget(self.aboutButton)

		self.finalLayout = QtGui.QVBoxLayout()
		self.finalLayout.addLayout(self.hLayout)
		self.finalLayout.addLayout(self.vLayout)
		self.setLayout(self.finalLayout)
		self.setWindowTitle("FATHOM")

		self.aboutButton.clicked.connect(self.aboutClicked)

	def setDistance(self, distance):
		distanceString = str(distance)
		self.distanceLabel.setText("Distance: " + distanceString)

	
	def setData(self, data):
		item = QtGui.QListWidgetItem
		self.dataList.setText(data)

	def aboutClicked(self):
		aboutMsgBox = QtGui.QMessageBox()
		aboutMsgBox.setText('Can you FATHOM it? A tapeless tape measure!')
		aboutMsgBox.setWindowTitle("FATHOM")
		aboutMsgBox.exec_()
