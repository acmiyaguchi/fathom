import sys
from PyQt4 import QtGui
from PyQt4 import QtCore
from MainWindow import MainWindow
from SplashScreen import SplashScreen

def main():
	app = QtGui.QApplication(sys.argv)

	startwindow = SplashScreen(sys.argv[1])
	startwindow.show()
	mainwindow = MainWindow()
	mainwindow.resize(300, 350)
	mainwindow.move(500, 0)

	QtCore.QTimer.singleShot(5000, startwindow.close)	
	QtCore.QTimer.singleShot(5000, mainwindow.show)

	sys.exit(app.exec_())


if __name__ == '__main__':
	main()

