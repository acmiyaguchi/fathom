import sys
from PyQt4 import QtGui

class SplashScreen(QtGui.QWidget):
	def __init__(self, imagename):
		super(SplashScreen, self).__init__()
		self.image = QtGui.QLabel(self)
		self.image.setPixmap(QtGui.QPixmap("resources/" + imagename))
		self.image.setPixmap(self.image.pixmap().scaled(300,350))
		self.image.adjustSize()
		self.move(500, 0)
		self.image.setScaledContents(True)
		self.setWindowTitle(" ")
